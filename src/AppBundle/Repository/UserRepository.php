<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 14.10.17
 * Time: 20:59
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Repository;

class UserRepository extends EntityRepository
{
    private $em;

    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        $this->em = $em;
        parent::__construct($em, $class);
    }

    public function addUser(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();
    }

    public function countMembers() : int
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}