<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 01.11.17
 * Time: 11:05
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;


class MessageRepository extends EntityRepository
{
    private $em;

    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        $this->em = $em;
        parent::__construct($em, $class);
    }

    public function add(Message $message)
    {
        $this->em->persist($message);
        $this->em->flush();
    }

    public function getConversations(User $user)
    {
        $q = $this->createQueryBuilder('q');
        $q->from('AppBundle:Conversation', 'c')
        ->select('c');

        return $q->getQuery()->getResult();

    }

    /**
     * @param int $id
     * @param User $user
     * @return mixed
     */
    public function getConversation(int $id, User $user)
    {
        $qbConversation = $this->createQueryBuilder('q');
        $qbConversation->from('AppBundle:Conversation', 'c')
            ->where('c.id = :id')
            ->andWhere('c.first_user = :user')
            ->orWhere('c.second_user = :user')
            ->setParameter(':user', $user)
            ->setParameter(':id', $id)
            ->select('c');
        return $qbConversation->getQuery()->getSingleResult();
    }


}