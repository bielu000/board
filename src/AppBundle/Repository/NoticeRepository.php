<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 23.10.17
 * Time: 22:46
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Notice;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

class NoticeRepository extends EntityRepository
{
    private $em;

    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        $this->em = $em;
        parent::__construct($em, $class);
    }

    public function addNotice(Notice $notice) : void
    {
//        foreach ($notice->getPhotos() as $p) { // DO wywalenia PRZEROBIC CHOCBY NIE WIEM CO! ! !
//            $this->em->persist($p);
//        }

        $this->em->persist($notice);
        $this->em->flush();
    }

    public function updateNotice($notice)
    {
        $this->em->merge($notice);
        $this->em->flush();
    }

    public function removeNotice(int $id)
    {
        $notice = $this->em->find('AppBundle:Notice', $id);

        $this->em->remove($notice);
        $this->em->flush();
    }

    public function countNotices()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');

        return $qb->getQuery()->getSingleScalarResult();;
    }

    public function getTopCategories()
    {
        $qb = $this->createQueryBuilder('a');

        return $qb->select('COUNT(a) as amount , c.name')
                ->join('a.category', 'c')
                ->addGroupBy('c.name')
                ->addOrderBy('amount', 'desc')
                ->getQuery()
                ->getResult();
    }

    public function browseNotice(string $search = null)
    {
        $qb = $this->createQueryBuilder('a');

        $qb->select('a' );

        if ($search != null) {
            $qb->where($qb->expr()->like('a.title', ':title'))
                ->setParameter('title', '%'.$search.'%');
        }

        $qb->orderBy('a.created_at', 'DESC');

        return $qb->getQuery();

    }
}