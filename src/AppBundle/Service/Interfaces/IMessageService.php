<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 01.11.17
 * Time: 11:14
 */

namespace AppBundle\Service\Interfaces;


interface IMessageService
{
    public function getConversations();
    public function getConversation(int $id);

}