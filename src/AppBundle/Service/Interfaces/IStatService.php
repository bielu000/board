<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 29.10.17
 * Time: 13:53
 */

namespace AppBundle\Service\Interfaces;


interface IStatService
{
    public function getNumberOfNotices();
    public function getNumberOfMembers();
}