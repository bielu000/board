<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 23.10.17
 * Time: 22:44
 */

namespace AppBundle\Service\Interfaces;


use AppBundle\Entity\Notice;

interface INoticeService
{
    public function getNotice($id);
    public function browseNotice(int $page, int $limit, string $title = null);
    public function createNotice(Notice $notice);
    public function editNotice(Notice $notice);
    public function removeNotice(int $id);
    public function getMyNotices();
    public function getTopCategories();
}