<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 23.10.17
 * Time: 22:45
 */

namespace AppBundle\Service;


use AppBundle\Entity\Notice;
use AppBundle\Repository\NoticeRepository;
use AppBundle\Service\Interfaces\INoticeService;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAware;
use Knp\Component\Pager\Paginator;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class NoticeService implements INoticeService
{
    private $repository;
    private $security;
    private $paginator;

    public function __construct(NoticeRepository $noticeRepository, $securityContext, Paginator $paginator)
    {
        $this->repository = $noticeRepository;
        $this->security = $securityContext;
        $this->paginator = $paginator;
    }

    public function getNotice($id)
    {
        $notice = $this->repository->find($id);

        if ($notice == null)
        {
            throw new NotFoundHttpException("Brak ogłoszenia dla o id $id");
        }

        return $notice;
    }
    public function browseNotice(int $page, int $limit, string $search = null)
    {
        $notices = $this->repository->browseNotice($search);
        $paginator = $this->paginator;
        $pagination = $paginator->paginate(
            $notices,
            $page,
            $limit
        );

        return $pagination;
    }

    public function createNotice(Notice $notice)
    {
        $security = $this->security;
        $repository = $this->repository;

        $notice->setOwner($security->getToken()->getUser());
        $notice->setIsActive(true);
        $notice->setCreatedAt(new \DateTime());
        $repository->addNotice($notice);
    }

    public function editNotice(Notice $notice)
    {
        $repository = $this->repository;
        $repository->updateNotice($notice);
    }

    public function removeNotice(int $id)
    {
        $repository = $this->repository;
        $security = $this->security;
        $user = $security->getToken()->getUser();

        $notice = $repository->find($id);

        if ($notice->getowner() !== $user)
        {
            return;
        }
        $repository->removeNotice($id);
    }

    public function getMyNotices()
    {
        $repository = $this->repository;
        $security = $this->security;
        $user = $security->getToken()->getUser();

        return $repository->findBy(['owner' => $user]);
    }


    public function getTopCategories()
    {
        return $this->repository->getTopCategories();
    }
}