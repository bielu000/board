<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 29.10.17
 * Time: 13:55
 */

namespace AppBundle\Service;


use AppBundle\Repository\NoticeRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\Interfaces\IStatService;

class StatService implements IStatService
{
    private $noticeRepository;
    private $userRepository;

    public function __construct(NoticeRepository $noticeRepository, UserRepository $userRepository)
    {
        $this->noticeRepository = $noticeRepository;
        $this->userRepository = $userRepository;
    }

    public function getNumberOfNotices()
    {
        return $this->noticeRepository->countNotices();
    }

    public function getNumberOfMembers()
    {
        return $this->userRepository->countMembers();
    }
}