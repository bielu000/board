<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 01.11.17
 * Time: 11:13
 */

namespace AppBundle\Service;


use AppBundle\Repository\MessageRepository;
use AppBundle\Service\Interfaces\IMessageService;

class MessageService implements IMessageService
{
    private $messageRepository;
    private $securityContext;

    /**
     * MessageService constructor.
     * @param MessageRepository $messageRepository
     * @param $securityContext
     */
    public function __construct(MessageRepository $messageRepository, $securityContext)
    {
        $this->messageRepository = $messageRepository;
        $this->securityContext = $securityContext;
    }

    /**
     * @return array
     */
    public function getConversations()
    {
        $security = $this->securityContext;
        $repository = $this->messageRepository;
        $user = $security->getToken()->getUser();

        $conversations = $repository->getConversations($user);

        return $conversations;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getConversation(int $id)
    {
        $security = $this->securityContext;
        $repository = $this->messageRepository;
        $user = $security->getToken()->getUser();

        return $repository->getConversation($id, $user);
    }

}