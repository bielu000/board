<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 26.10.17
 * Time: 21:06
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Notice;
use AppBundle\Entity\Photo;
use AppBundle\Form\NoticeType;
use AppBundle\Service\Interfaces\INoticeService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class NoticeController extends Controller
{
    private $noticeService;

    public function __construct(INoticeService $noticeService)
    {
        $this->noticeService = $noticeService;
    }

    /**
     * @Route("/notice/{id}", name="notice_details", requirements={"id": "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getNoticeAction($id)
    {
        $service = $this->noticeService;

        $notice = $service->getNotice($id);

        return $this->render('notice/notice-details.html.twig', ['notice' => $notice]);
    }

    /**
     * @Route("/notice/create", name="create_notice")
     * @param \HttpRequest|Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createNoticeAction(Request $request)
    {
        $service = $this->noticeService;
        $form = $this->createForm(NoticeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $notice = $form->getData();

            foreach ($notice->getAttachments() as $file)
            {
                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('notice_photos_directory'),
                    $fileName
                );

                //DO ZMIANY

                $photo = new Photo();
                $photo->setNotice($notice);
                $photo->setName($fileName);
                $photo->setPath($fileName);

                $notice->getPhotos()->add($photo);
            }


            $service->createNotice($notice);
            $this->addFlash(
                'success',
                'Ogłoszenie zostało dodane'
            );

            return$this->redirectToRoute('board');
        }

        return $this->render('notice/create-notice.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit_notice", requirements={"id": "\d+"})
     * @param $id
     *
     */
    public function editNoticeAction(Request $request, $id)
    {
        $service = $this->noticeService;
        $notice = $service->getNotice($id);
        $form = $this->createForm(NoticeType::class, $notice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $notice = $form->getData();
            $service->editNotice($notice);
            $this->addFlash(
                'success',
                'Zmiany zostały zapisane'
            );

            return $this->redirectToRoute('account', ['_fragment' => 'asd']);
        }

        return $this->render('notice/edit-notice.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/remove/{id}", name="remove_notice", requirements={"id": "\d+"}, condition="request.isXmlHttpRequest()")
     * @param $id
     * @return JsonResponse
     */
    public function removeNoticeAction($id)
    {
        $service = $this->noticeService;
        $service->removeNotice($id);

        return new JsonResponse(
            ['status' => 1]
        );
    }

    /**
     * @Route("/my_notices", name="my_notices", condition="request.isXmlHttpRequest()")
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param \HttpRequest|Request $request
     */
    public function getMyNotices()
    {
        $service = $this->noticeService;

        $notices = $service->getMyNotices();

        return $this->render('notice/my-notices.html.twig', ['notices' => $notices]);
    }
}