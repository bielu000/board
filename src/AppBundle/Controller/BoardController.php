<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 23.10.17
 * Time: 22:43
 */

namespace AppBundle\Controller;

use AppBundle\Service\Interfaces\IStatService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Service\Interfaces\INoticeService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BoardController extends Controller
{
    private $noticeService;
    private $statService;

    public function __construct(INoticeService $noticeService, IStatService $statService)
    {
        $this->noticeService = $noticeService;
        $this->statService = $statService;
    }

    /**
     * @Route("/board", name="board")
     */
    public function indexAction(Request $request)
    {
        $noticeService = $this->noticeService;
        $statService = $this->statService;
        $pagination = $noticeService->browseNotice(
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 3),
            $request->get('title'));

        return $this->render('board/board.html.twig', [
            'notices' => $pagination,
            'noticeNumbers' => $statService->getNumberOfNotices(),
            'memberNumbers' => $statService->getNumberOfMembers(),
            'topCategories' => $noticeService->getTopCategories()
        ]);
    }

}
