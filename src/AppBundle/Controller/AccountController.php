<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 15.10.17
 * Time: 22:49
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends Controller
{
    /**
     * @Route("/account", name="account")
     */
    public function indexAction(Request $request)
    {
        return $this->render('account/account.html.twig');
    }

    /**
     * @Route("/account_info", name="account_info")
     */
    public function getAccountInfo()
    {
        return $this->render('account/account-info.html.twig');
    }

    /**
     * @Route("/change_password", name="change_password")
     */
    public function changePasswordAction(Request $request)
    {

        return new Response("Zmiana hasła");
    }


}