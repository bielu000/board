<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 01.11.17
 * Time: 11:16
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Service\Interfaces\IMessageService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller
{
    private $messageService;

    public function __construct(IMessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    /**
     * @Route("/inbox", name="inbox", condition="request.isXmlHttpRequest()")
     */
    public function getMessagesAction()
    {
        $service = $this->messageService;

        $con = $service->getConversations();

        return $this->render('messages/conversations.html.twig', ['conversations' => $con]);
    }

    /**
     * @Route("/conversation/{id}", requirements={"id": "\d+"}, name="conversation", condition="request.isXmlHttpRequest()")
     * @param int $id
     * @return Response
     */
    public function getConversationAction($id)
    {
        $service = $this->messageService;

        $singleConversation = $service->getConversation($id);

        return $this->render('messages/conversation.html.twig', ['conversation' => $singleConversation]);
    }



}