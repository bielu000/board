<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 01.11.17
 * Time: 11:30
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;

class Conversation
{
    private $id;
    private $first_user;
    private $second_user;
    private $messages;


    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getFirstUser()
    {
        return $this->first_user;
    }

    /**
     * @param mixed $first_user
     */
    public function setFirstUser($first_user)
    {
        $this->first_user = $first_user;
    }

    /**
     * @return mixed
     */
    public function getSecondUser()
    {
        return $this->second_user;
    }

    /**
     * @param mixed $second_user
     */
    public function setSecondUser($second_user)
    {
        $this->second_user = $second_user;
    }
    private $isActive;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getisActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getNotices()
    {
        return $this->notices;
    }

    /**
     * @param mixed $notices
     */
    public function setNotices($notices)
    {
        $this->notices = $notices;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

}