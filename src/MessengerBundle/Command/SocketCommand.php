<?php
/**
 * Created by PhpStorm.
 * User: patryk
 * Date: 05.11.17
 * Time: 15:51
 */

namespace MessengerBundle\Command;


use MessengerBundle\Service\Messenger;
use MessengerBundle\Service\Pusher;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;
use React\Socket\Server;
use React\ZMQ\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ZMQ;

class SocketCommand extends Command
{
    protected function configure()
    {
        $this->setName('sockets:start-messenger')
            // the short description shown while running "php bin/console list"
            ->setHelp("Starts the chat socket demo")
            // the full command description shown when running the command with
            ->setDescription('Starts the chat socket demo')
        ;
    }
//    protected function execute(InputInterface $input, OutputInterface $output)
//    {
//        $output->writeln([
//            'Chat socket',// A line
//            '============',// Another line
//            'Starting chat, open your browser.',// Empty line
//        ]);
//
//        $server = IoServer::factory(
//            new HttpServer(
//                new WsServer(
//                    new Messenger()
//                )
//            ),
//            8080
//        );
//
//        $server->run();
//    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected  function execute(InputInterface $input, OutputInterface $output)
    {
          $loop   = Factory::create();
          $pusher = new Pusher();

          // Listen for the web server to make a ZeroMQ push after an ajax request
          $context = new Context($loop);
          $pull = $context->getSocket(ZMQ::SOCKET_PULL);
          $pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
          $pull->on('message', array($pusher, 'onBlogEntry'));

          // Set up our WebSocket server for clients wanting real-time updates
          $webSock = new Server('0.0.0.0:8080', $loop); // Binding to 0.0.0.0 means remotes can connect
          $webServer = new IoServer(
              new HttpServer(
                  new WsServer(
                      new WampServer(
                          $pusher
                      )
                  )
              ),
              $webSock
          );

          $loop->run();
    }
}