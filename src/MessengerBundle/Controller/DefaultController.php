<?php

namespace MessengerBundle\Controller;

use Ratchet\Wamp\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ZMQContext;

class DefaultController extends Controller
{
    /**
     * @Route("/messenger-test")
     */
    public function indexAction()
    {
        phpinfo();
//        $entryData = array(
//            'category' => $_POST['category']
//        , 'title'    => $_POST['title']
//        , 'article'  => $_POST['article']
//        , 'when'     => time()
//        );


        var_dump(class_exists('ZMQContext'));

//            $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
//            $socket->connect("tcp://localhost:5555");
//
//            $socket->send(json_encode($entryData));

        return $this->render('MessengerBundle:Default:index.html.twig');
    }
}
