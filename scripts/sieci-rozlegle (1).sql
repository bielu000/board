-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 09 Sty 2018, 13:47
-- Wersja serwera: 10.1.26-MariaDB
-- Wersja PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sieci-rozlegle`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL COMMENT 'id kategorii',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nazwa kategorii'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Samochody'),
(2, 'AGD'),
(3, 'Komputery PC'),
(4, 'Laptopy'),
(5, 'Smartfony'),
(6, 'TV'),
(7, 'Motocykle'),
(8, 'Meble');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `conversation`
--

CREATE TABLE `conversation` (
  `id` int(11) NOT NULL COMMENT 'conversation id',
  `first_user` int(11) DEFAULT NULL COMMENT 'id użytkownika',
  `second_user` int(11) DEFAULT NULL COMMENT 'id użytkownika'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `conversation`
--

INSERT INTO `conversation` (`id`, `first_user`, `second_user`) VALUES
(3, 56, 58),
(4, 56, 59);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL COMMENT 'message id',
  `sender_id` int(11) DEFAULT NULL COMMENT 'id użytkownika',
  `content` varchar(800) COLLATE utf8_unicode_ci NOT NULL COMMENT 'message content',
  `sent_at` datetime NOT NULL COMMENT 'sent date',
  `conversation_id` int(11) DEFAULT NULL COMMENT 'conversation id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `message`
--

INSERT INTO `message` (`id`, `sender_id`, `content`, `sent_at`, `conversation_id`) VALUES
(10, 56, 'Pierwsza wiadomosc', '2017-11-01 00:00:00', 3),
(11, 56, 'Druga wiadomosc', '2017-11-02 00:00:00', 4),
(12, 56, 'Pierwsza druga wiadomosc', '2017-11-03 00:00:00', 3),
(13, 58, 'asdasdasd', '2017-11-05 00:00:00', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL COMMENT 'notice id',
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'imię',
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Opis',
  `content` varchar(900) COLLATE utf8_unicode_ci NOT NULL COMMENT 'notice description',
  `created_at` datetime NOT NULL COMMENT 'created at date',
  `is_active` tinyint(1) NOT NULL,
  `id_user` int(11) DEFAULT NULL COMMENT 'id użytkownika',
  `category_id` int(11) DEFAULT NULL COMMENT 'id kategorii',
  `price` double NOT NULL COMMENT 'price',
  `type` enum('Sprzedam','Kupię','Zamienię') COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `notice`
--

INSERT INTO `notice` (`id`, `title`, `description`, `content`, `created_at`, `is_active`, `id_user`, `category_id`, `price`, `type`) VALUES
(24, 'aso d;iash d;oiahs dias d\'iahs d\'aos', 'asdasd', 'asdasdasdasdasd', '2017-10-29 15:17:17', 1, 58, 1, 340, 'Sprzedam'),
(53, 'asdasdasdasd', 'sad', 'asd', '2017-10-30 23:12:33', 1, 55, 1, 123, 'Sprzedam');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL COMMENT 'id użytkownika',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name',
  `path` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Path',
  `id_notice` int(11) DEFAULT NULL COMMENT 'notice id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL COMMENT 'id użytkownika',
  `userName` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'imię',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email użytkownika',
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'password',
  `salt` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'salt',
  `is_active` tinyint(1) NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'imię',
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nazwisko',
  `gender` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'płeć',
  `birthday` date NOT NULL COMMENT 'płeć'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`idUser`, `userName`, `email`, `password`, `salt`, `is_active`, `first_name`, `last_name`, `gender`, `birthday`) VALUES
(55, 'admin', 'admin', 'sqzVkrUDH/DuHhYIrNjREFf8KuWYCWDVuhThJ1hpC5jOw59IWlsVew==', 'zVWVxQY1vxFa4g==', 1, 'admin', 'admin', 'male', '1994-03-17'),
(56, 'bielu000', 'bielu000@o2.pl', 'hEV5Rap1Csjk3koXIcFr/kFDyn51y4AXEeElql8ak5nN+cs9VkvhNQ==', 'WQUVndODSgXd+g==', 1, 'Patryk', 'Biel', 'male', '1994-03-17'),
(57, 'qwe', 'qwe', 'lpLHGNBCzKFQX4+horgWKr3ePtf3JkasQYRQ2gXVJqxdq3wqkeKE8w==', 'wAhkpaysuWWeQA==', 1, 'qwe', 'qwe', 'male', '1994-03-17'),
(58, 'bielu', 'asd', '8CwYbB1YKSrKatzMjAhuXS/L9giCHH+xaApVjAsmi3INGPUb+TZBqA==', 'JBv8HafyqA8sOQ==', 1, 'Anna ', 'Janusz', 'female', '1994-03-17'),
(59, 'abc', 'abc', 't8zGY6JniVhO+4DG4hrwAAUG2ukKlZU3rV7NW37pgo+X6Riepy5wIw==', 'paOykRa94YzVpQ==', 1, 'abc', 'abc', 'male', '2017-09-28'),
(60, 'pepe', 'pepe@o2.pl', 'IdNuP90GXpN0Kptd3Gwu30amZWtvazXsKy/m5Tr8wNwxrBT5uXKoqA==', '8tg2ovp/YY8Wow==', 1, 'Patryk', 'Biel', 'male', '2004-03-02');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8A8E26E97A61697D` (`first_user`),
  ADD KEY `IDX_8A8E26E99D46EE7` (`second_user`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FF624B39D` (`sender_id`),
  ADD KEY `IDX_B6BD307F9AC0396` (`conversation_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_480D45C26B3CA4B` (`id_user`),
  ADD KEY `IDX_480D45C212469DE2` (`category_id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_14B784186E3AF02F` (`id_notice`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id kategorii', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'conversation id', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'message id', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT dla tabeli `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'notice id', AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT dla tabeli `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id użytkownika', AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id użytkownika', AUTO_INCREMENT=61;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `conversation`
--
ALTER TABLE `conversation`
  ADD CONSTRAINT `FK_8A8E26E97A61697D` FOREIGN KEY (`first_user`) REFERENCES `user` (`idUser`),
  ADD CONSTRAINT `FK_8A8E26E99D46EE7` FOREIGN KEY (`second_user`) REFERENCES `user` (`idUser`);

--
-- Ograniczenia dla tabeli `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307F9AC0396` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`),
  ADD CONSTRAINT `FK_B6BD307FF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `user` (`idUser`);

--
-- Ograniczenia dla tabeli `notice`
--
ALTER TABLE `notice`
  ADD CONSTRAINT `FK_480D45C212469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_480D45C26B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `user` (`idUser`);

--
-- Ograniczenia dla tabeli `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `FK_14B784186E3AF02F` FOREIGN KEY (`id_notice`) REFERENCES `notice` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
